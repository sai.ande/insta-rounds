import {Component, OnInit, Injectable} from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';
import {HomeService} from '../../services/home.service';
import {first, map, tap} from 'rxjs/operators';
import {pipe} from 'rxjs';

declare var moment: any;

@Component({
  selector: 'app-anlytiks',
  templateUrl: './anlytiks.component.html',
  styleUrls: ['./anlytiks.component.scss']
})
@Injectable()
export class AnlytiksComponent<D> implements OnInit {
  customAlertOptions: any = {
    header: 'Choose One',
    translucent: true
  };
  patientTypeList = ['OB', 'GYN'];
  patientStatusList = ['IN', 'OUT'];
  babyAPGARLes = '<7';
  babyAPGARGa = '>7';
  EBLLes = '<100 cc';
  EBLGa = '>700 cc';
  minDate: any;
  fromDate: any;
  toDate: any;
  procedureType = '';
  analyticData = {
    patientTypeId: 'OB',
    admissionStatus: 'IN',
    url: '',
    fromDate: '',
    toDate: '',
    patientType: '',
    byYrMn: 'month',
    hospitalID: '',
    gbs: '',
    procedureTypesId: '',
    babyInfo: '',
    babyGender: '',
    apgar: '',
    liveBirth: '',
    inducedReason: '',
    fcm: '',
    ebl: '',
    postop: '',
    statType: 'avg',
    csecReason: '',
    procedureGYNTypesId: ''
  };
  hospitalList = [];
  obProcedureList = [];
  gynProcedureList = [];
  inReason = [];
  csectionReasonList = [];
  selectedInduse = [];
  selectedProcedure = [];
  constructor(private router: Router, private homeService: HomeService, ) {
    this.minDate = moment(Date.now()).format('YYYY-MM-DD');
    this.analyticData.toDate = moment(Date.now()).format('YYYY-MM-DD');
    const date = moment(Date.now()).subtract(180, 'days');
    this.analyticData.fromDate = moment(date).format('YYYY-MM-DD');
  }
  ngOnInit(): void {
    this.getHospitals();
    this.getProcedures();
  }

  getHospitals() {
    // this.appService.showLoader();
    this.homeService.fetchHospitals().subscribe((data) => {
      if (data && data._statusCode === '200') {
        this.hospitalList = data.data.hospitalList;
        this.getProcedures();
      } else if (!data) {
        // this.appService.hideLoader();
        this.goToLoginScreen();
      } else {
        //  this.appService.hideLoader();
        // this.appService.alert('!Error', data.message);
      }
    });
  }
  getProcedures() {
    this.homeService.fetchProcedures(1).subscribe((data: any) => {
    // this.homeService.fetchProcedures(1).pipe(map((data: any) => {
      console.log(data.data);
      if (data && data._statusCode === '200') {
        this.obProcedureList = data.data.procedureList;
        this.inReason = data.data.inReason;
        this.csectionReasonList = data.data.csectionReasonList;
        this.getGYNProcedures();
      } else if (!data) {
       //  this.appService.hideLoader();
        this.goToLoginScreen();
      } else {
        // this.appService.hideLoader();
        // this.appService.alert('!Error', data.message);
      }
    });
  }

  getGYNProcedures() {
    this.homeService.fetchProcedures(2).subscribe((data: any) => {
      console.log(data.data);
      // this.appService.hideLoader();
      if (data && data._statusCode === '200') {
        this.gynProcedureList = data.data.procedureList;
      } else if (!data) {
        // this.appService.hideLoader();
        this.goToLoginScreen();
      } else {
        // this.appService.hideLoader();
        // this.appService.alert('!Error', data.message);
      }
    });
  }

  onAnlyticsChange(event: any) {
    this.analyticData.patientTypeId = event.target.value;
  }
  onPatientStatusChange(event: any) {
    this.analyticData.admissionStatus = event.target.value;
  }
  updateFromDate(event: any) {
    this.analyticData.fromDate = moment(event.target.value).format('YYYY-MM-DD');
  }
  updateToDate(event: any) {
    this.analyticData.toDate = moment(event.target.value).format('YYYY-MM-DD');
  }
  onbyChange(event: any) {
    this.analyticData.byYrMn = event.target.value;
  }
  onStatisticalChange(event: any) {
    this.analyticData.statType = event.target.value;
  }
  onhospitalChange(event: any) {
    this.analyticData.hospitalID = event.target.value;
  }
  onGBSChange(event: any) {
    this.analyticData.gbs = event.target.value;
  }
  onProcedureChange(event: any) {
    this.analyticData.procedureTypesId = event.target.value;
    this.obProcedureList.forEach((proce) => {
      if (proce.id === Number(event.target.value)) {
        this.procedureType = proce.procedureName;
      }
    });
  }
  onGYNProcedureChange(event: any) {
    this.selectedProcedure = event.target.value;
    this.selectedProcedure.forEach((proce, index) => {
      if (index === 0) {
        this.analyticData.procedureGYNTypesId = proce;
      } else {
        this.analyticData.procedureGYNTypesId = this.analyticData.procedureGYNTypesId + ',' + proce;
      }
    });
    if (this.selectedProcedure.length === 0) {
      this.analyticData.procedureGYNTypesId = '';
    }
    console.log(this.analyticData.procedureGYNTypesId);
  }
  CSectionReasonChange(event: any) {
    this.analyticData.csecReason = event.target.value;
  }
  inducedreasionTypeselect(event: any) {
    this.selectedInduse = event.target.value;
    this.selectedInduse.forEach((proce, index) => {
      if (index === 0) {
        this.analyticData.inducedReason = proce;
      } else {
        this.analyticData.inducedReason = this.analyticData.inducedReason + ',' + proce;
      }
    });
    if (this.selectedInduse.length === 0) {
      this.analyticData.inducedReason = '';
    }
    console.log(this.analyticData.inducedReason);
  }
  onBabyTypeChange(event: any) {
    this.analyticData.babyInfo = event.target.value;
  }
  onBabySexChange(event: any) {
    this.analyticData.babyGender = event.target.value;
  }
  onBabyApgarChange(event: any) {
    this.analyticData.apgar = event.target.value;
  }
  onlivebirthChange(event: any) {
    this.analyticData.liveBirth = event.target.value;
  }
  onpostopChange(event: any) {
    this.analyticData.postop = event.target.value;
  }
  onEblChange(event: any) {
    this.analyticData.ebl = event.target.value;
  }
  onCatheterChange(event: any) {
    this.analyticData.fcm = event.target.value;
  }
  getAnalytics() {
    if (this.analyticData.patientTypeId === '') {
     //  this.appService.alert('!Warning', 'Please enter patieny type.');
    } else if (this.analyticData.admissionStatus === '') {
      // this.appService.alert('!Warning', 'Please enter patient status.');
    } else if (this.analyticData.fromDate === '') {
      // this.appService.alert('!Warning', 'Please enter from date.');
    } else if (this.analyticData.toDate === '') {
      // this.appService.alert('!Warning', 'Please enter to date.');
    } else {
      const time1 = moment(this.analyticData.fromDate).format('YYYY-MM-DD');
      const time2 = moment(this.analyticData.toDate).format('YYYY-MM-DD');
      if (time1 > time2) {
        // this.appService.alert('!Warning', 'Please enter the from date is less than to date.');
      } else {
        this.submitData();
      }
    }
  }

  submitData() {
    if (this.analyticData.patientTypeId === 'OB') {
      this.analyticData.patientType = '1';
    } else if (this.analyticData.patientTypeId === 'GYN') {
      this.analyticData.patientType = '2';
    }
    this.analyticData.fromDate = moment(this.analyticData.fromDate).format('YYYY/MM/DD');
    this.analyticData.toDate = moment(this.analyticData.toDate).format('YYYY/MM/DD');

    console.log(this.analyticData.inducedReason);
    const obDetails = {
      token: localStorage.getItem('deviceToken'),
      fromDate: this.analyticData.fromDate,
      toDate: this.analyticData.toDate,
      byYrMn: this.analyticData.byYrMn,
      statType: this.analyticData.statType,
      patientTypeId: this.analyticData.patientType,
      admissionStatus: this.analyticData.admissionStatus,
      hospitalID: this.analyticData.hospitalID.toString(),
      gbs: this.analyticData.gbs,
      procedureTypesId: this.analyticData.procedureTypesId.toString(),
      inducedReason: this.analyticData.inducedReason,
      babyInfo: this.analyticData.babyInfo,
      babyGender: this.analyticData.babyGender,
      apgar: this.analyticData.apgar,
      liveBirth: this.analyticData.liveBirth,
      csecReason: this.analyticData.csecReason,
      chartType: 'pie'
    };

    const gynDetails = {
      token: localStorage.getItem('deviceToken'),
      fromDate: this.analyticData.fromDate,
      toDate: this.analyticData.toDate,
      byYrMn: this.analyticData.byYrMn,
      statType: this.analyticData.statType,
      patientTypeId: this.analyticData.patientType,
      admissionStatus: this.analyticData.admissionStatus,
      hospitalID: this.analyticData.hospitalID.toString(),
      postop: this.analyticData.postop,
      ebl: this.analyticData.ebl,
      fcm: this.analyticData.fcm,
      procedureTypesId: this.analyticData.procedureGYNTypesId,
      chartType: 'pie'
    };

    let obj = {};
    if (this.analyticData.patientTypeId === 'OB') {
      obj = obDetails;
    } else if (this.analyticData.patientTypeId === 'GYN') {
      obj = gynDetails;
    }
    this.homeService.fetchAnlyticsData(obj).pipe(first()).subscribe((data: any) => {
      console.log(data.data);
      // this.appService.hideLoader();
      if (data.status) {
        console.log(data.data.data);
        console.log(data.data.data.pie[1]);
        if (data.data.data.pie[1].series.length > 0 && data.data.data.bar[0].series.xAxis.length > 0) {
          const navigationExtras: NavigationExtras = {
            state: {
              seriesData: data.data.data,
            }
          };
          this.router.navigate(['chartsview'], navigationExtras);
        } else {
          // this.appService.alert('!Warning', 'No data found for your selected dates.');
        }
      } else if (!data.status) {
        this.goToLoginScreen();
      } else {
        // this.appService.alert('!Error', data.message);
      }
    });
  }

  goToLoginScreen() {
    localStorage.setItem('deviceToken', '');
    localStorage.setItem('userData', '');
    localStorage.setItem('deviceId', '');
    this.router.navigateByUrl('/login');
  }

}
