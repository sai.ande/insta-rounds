import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {User} from '../model/user';
import {environment} from '../../environments/environment';
import {resolve} from 'url';


@Injectable({ providedIn: 'root' })
export class HomeService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient) {

  }

  fetchPatientCounts() {
    return this.http.get(`${environment.api.baseUrl}patient/getPatCount?token=${localStorage.getItem('deviceToken')}&patSearch=false`)
      .pipe(
        tap(_ => this.log('fetched users')
        ),
        catchError(this.handleError<any>('getUserTimesheet', []))
      );
  }

  fetchHospitals(): Observable<any> {
    return this.http.get(`${environment.api.baseUrl}provider/gethospitals?token=${localStorage.getItem('deviceToken')}`);

  }

  fetchProcedures(patType: number) {
    return this.http.get(`${environment.api.baseUrl}provider/getprocedures?token=${localStorage.getItem('deviceToken')}&patTypeId=${patType}`);
  }

  fetchSchedulerData() {
    return this.http.get(`${environment.api.baseUrl}provider/scheduleData?token=${localStorage.getItem('deviceToken')}`).pipe(map(data => {})).subscribe(result => {
        return result;
      },
      error => {
        return error;
      });
  }

  fetchAdmitPatient(userData: any) {
    return this.http.put(`${environment.api.baseUrl}patient/admitPatient?token=${localStorage.getItem('deviceToken')}`, userData)
      .pipe(
        tap(_ => this.log('fetched users')
        ),
        catchError(this.handleError<any>('getUserTimesheet', []))
      );
  }

  fetchReAdmitPatient(userData: any) {
    return this.http.put(`${environment.api.baseUrl}patient/reAdmitPatient}`, userData)
      .pipe(
        tap(_ => this.log('fetched users')
        ),
        catchError(this.handleError<any>('getUserTimesheet', []))
      );
  }

  fetchDischargePatient(userData: any) {
    return this.http.put(`${environment.api.baseUrl}patient/dischargePatient}`, userData)
      .pipe(
        tap(_ => this.log('fetched users')
        ),
        catchError(this.handleError<any>('getUserTimesheet', []))
      );
  }

  fetchUnAdimtPatient(userData: any) {
    return this.http.put(`${environment.api.baseUrl}patient/unadmitPatient}`, userData)
      .pipe(
        tap(_ => this.log('fetched users')
        ),
        catchError(this.handleError<any>('getUserTimesheet', []))
      );

  }

  submitPatientDetails(endPoinr: any, patientData: any) {
    return this.http.put(`${environment.api.baseUrl}apt/patientApptUpdate}`, patientData)
      .pipe(
        tap(_ => this.log('fetched users')
        ),
        catchError(this.handleError<any>('getUserTimesheet', []))
      );
  }

  submitAppoinmentDetails(endPoinr: any, patientData: any) {
    return this.http.put(`${environment.api.baseUrl}apt/patientAppointment}`, patientData)
      .subscribe((apiResponse: any) => {
        if (apiResponse && apiResponse._statusCode === '200') {
          resolve({ status: true, data: apiResponse.data, statusCode: apiResponse._statusCode });
        } else if (apiResponse._statusCode === '412') {
          resolve({ status: false, statusCode: apiResponse._statusCode });
        } else {
          console.log(
            'fetchHospitals',
            `Error @fetchProcedures: ${JSON.stringify(apiResponse)}`
          );
          resolve({ status: false, message: apiResponse._statusMessage });
        }

      }),
      catchError(this.handleError<any>('fetchReAdmitPatient', []));


  }

  fetchAnlyticsData(data: any) {
    return this.http.post(`${environment.api.baseUrl}analytics/getADcounts`, data)
      .pipe(
        tap(_ => this.log('fetched users')
        ),
        catchError(this.handleError<any>('getUserTimesheet', []))
      );
  }

  private handleError<T>(operation = 'operation', result?: any[]) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as unknown as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }
}
